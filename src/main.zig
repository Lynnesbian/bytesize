const std = @import("std");
const testing = std.testing;
const test_str = std.testing.expectEqualStrings;

// kilo, mega, giga...
// including anything beyond exabytes causes an overflow in compute_thresholds
const prefixes = ".KMGTPE";
const si_prefixes = [_][] const u8{"", "kilo", "mega", "giga", "tera", "peta", "exa"};
const bin_prefixes = [_][] const u8{"", "kibi", "mebi", "gibi", "tebi", "pebi", "exbi"};

/// Computes two arrays of size thresholds at compile time. The returned arrays will look something like:
/// [1, 1024, 1024^2, 1024^3...]
/// [1, 1000, 1000^2, 1000^3...]
fn compute_thresholds() [2][prefixes.len]u64 {
  comptime {
    var results = [2][prefixes.len]u64{.{0} ** prefixes.len, .{0} ** prefixes.len};
    var i = 1;
    inline while (i < prefixes.len) : (i += 1) {
      results[0][i] = std.math.pow(u64, 1024, i);
      results[1][i] = std.math.pow(u64, 1000, i);
    }

    return results;
  }
}

/// Given an allocator, a size in bytes, and flags for whether to use SI values and long results, returns the
/// given number of bytes as a allocated string, formatted accordingly.
/// For example:
///   format_size(alloc, 1024, true, false) -> "1.00 KiB"
///   format_size(alloc, 1000, false, true) -> "1.00 kilobytes"
fn format_size(alloc: std.mem.Allocator, size: u64, si: bool, long: bool) ![]u8 {
  const thresholds = compute_thresholds()[if (si) 1 else 0];
  var i: usize = 1;

  // find which threshold `size` falls under
  while (size >= thresholds[i]) : (i += 1) {}
  if (i == 1) {
    // `size` is less than the kibibyte/kilobyte threshold - just print "123 bytes"
    const suffix: []const u8 = if (long) "bytes" else "B";
    return std.fmt.allocPrint(alloc, "{d} {s}", .{size, suffix});
  } else {
    i -= 1;

    const long_prefix: []const u8 = if (si) si_prefixes[i] else bin_prefixes[i];
    // there are three possibilities for the suffix:
    //  - if printing the long version, it should always be "bytes" (e.g. kilobytes, kibibytes...)
    //  - if printing the short SI version, it should be "B" (e.g. KB)
    //  - if printing the short binary version, it should be "iB" (e.g. KiB)
    const suffix: []const u8 = if (long) "bytes" else if (si) "B" else "iB";

    return try std.fmt.allocPrint(
      alloc,
      // print something like "1.23 KiB" or "1.23 kibibytes"
      "{d:.2} {s}{s}",
      .{
        @intToFloat(f64, size) / @intToFloat(f64, thresholds[i]),
        if (long) long_prefix else prefixes[i..i+1],
        suffix
      }
    );
  }
}

test "binary test - short" {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();
  const alloc = arena.allocator();

  try test_str("0 B", try format_size(alloc, 0, false, false));
  try test_str("1023 B", try format_size(alloc, 1023, false, false));
  try test_str("1.00 KiB", try format_size(alloc, 1025, false, false));
  try test_str("1.50 KiB", try format_size(alloc, 1536, false, false));
  try test_str("1.00 MiB", try format_size(alloc, 1024 * 1024, false, false));
  try test_str("1.50 GiB", try format_size(alloc, (1024 * 1024 * 1024) * 1.5, false, false));
}

test "binary test - long" {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();
  const alloc = arena.allocator();

  try test_str("0 bytes", try format_size(alloc, 0, false, true));
  try test_str("1023 bytes", try format_size(alloc, 1023, false, true));
  try test_str("1.00 kibibytes", try format_size(alloc, 1025, false, true));
  try test_str("1.00 mebibytes", try format_size(alloc, 1024 * 1024, false, true));
}

test "si test - short" {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();
  const alloc = arena.allocator();

  try test_str("0 B", try format_size(alloc, 0, true, false));
  try test_str("999 B", try format_size(alloc, 999, true, false));
  try test_str("1.00 KB", try format_size(alloc, 1000, true, false));
  try test_str("1.00 MB", try format_size(alloc, 1000 * 1000, true, false));
}

test "si test - long" {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();
  const alloc = arena.allocator();

  try test_str("0 bytes", try format_size(alloc, 0, true, true));
  try test_str("999 bytes", try format_size(alloc, 999, true, true));
  try test_str("1.00 kilobytes", try format_size(alloc, 1000, true, true));
  try test_str("1.00 megabytes", try format_size(alloc, 1000 * 1000, true, true));
}
